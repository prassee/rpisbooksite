package sample.distributeddata

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.Props
import akka.cluster.Cluster
import akka.cluster.ddata.DistributedData
import akka.cluster.ddata.LWWMap
import akka.cluster.ddata.LWWMapKey
import akka.actor.ActorSystem
import com.typesafe.config.ConfigFactory
import akka.actor.Address
import scala.concurrent.duration._

object ReplicatedCache {
  import akka.cluster.ddata.Replicator._

  def props: Props = Props[ReplicatedCache]

  private final case class Request(key: String, replyTo: ActorRef)

  final case class PutInCache(key: String, value: Any)
  final case class GetFromCache(key: String)
  final case class Cached(key: String, value: Option[Any])
  final case class Evict(key: String)
}

class ReplicatedCache extends Actor {
  import akka.cluster.ddata.Replicator._
  import ReplicatedCache._

  val replicator = DistributedData(context.system).replicator
  implicit val cluster = Cluster(context.system)

  def dataKey(entryKey: String): LWWMapKey[Any] =
    LWWMapKey("cache-" + math.abs(entryKey.hashCode) % 100)

  def receive = {
    case PutInCache(key, value) ⇒
      val writeMajority = WriteMajority(timeout = 2.seconds)
      replicator ! Update(dataKey(key), LWWMap(), writeMajority)(_ + (key -> value))
    case Evict(key) ⇒
      val writeMajority = WriteMajority(timeout = 2.seconds)
      replicator ! Update(dataKey(key), LWWMap(), writeMajority)(_ - key)
    case GetFromCache(key) ⇒
      val readMajority = ReadMajority(timeout = 2.seconds)
      replicator ! Get(dataKey(key), readMajority, Some(Request(key, sender())))
    case g @ GetSuccess(LWWMapKey(_), Some(Request(key, replyTo))) ⇒
      g.dataValue match {
        case data: LWWMap[_] ⇒ data.get(key) match {
          case Some(value) ⇒ println("Value for key: " + key + " is " + value)
          case None        ⇒ println("Not found")
        }
      }
    case NotFound(_, Some(Request(key, replyTo))) ⇒
      replyTo ! Cached(key, None)
    case _: UpdateResponse[_] ⇒ // ok
  }

}

object KeyValueClient {

  def main(args: Array[String]): Unit = {
    
    val port = args(0)
    val config = ConfigFactory.parseString(s"akka.remote.netty.tcp.port=$port").
      withFallback(ConfigFactory.load())

    val system = ActorSystem("ClusterSystem", config)
    implicit val cluster = Cluster(system)

    val address = new Address("akka.tcp", "ClusterSystem", "192.168.4.5", 2552)

    cluster.join(address)
    
    val cacheRef = system.actorOf(Props[ReplicatedCache])
    
    Thread.sleep(10000)
    cacheRef ! ReplicatedCache.PutInCache("movie1", "The Phantom Menace")
    cacheRef ! ReplicatedCache.PutInCache("movie2", "Attack of the clones")
    cacheRef ! ReplicatedCache.PutInCache("movie3", "Revenge of the sith")
    
    cacheRef ! ReplicatedCache.GetFromCache("movie1")
    cacheRef ! ReplicatedCache.GetFromCache("movie2")
    cacheRef ! ReplicatedCache.GetFromCache("movie3")
    
    Thread.sleep(2000)
    cacheRef ! ReplicatedCache.PutInCache("movie1", "A New Hope")
    cacheRef ! ReplicatedCache.GetFromCache("movie1")
    
    //Thread.sleep(10000)
    //cacheRef ! ReplicatedCache.GetFromCache("movie1")
    
  }
}