import scala.io.Source
import java.io.File
import scala.collection.mutable

object Indexer {

  def readFile(path: String, skipLines: Int): Iterator[String] = {
    Source.fromFile(new File(path)).getLines.drop(skipLines)
  }

  def stringToTuple2(str: String): Option[(String, String)] = {
    val words = str.split(" ")

    words.length match {
      case len if (len > 1) => Some(words(0), words(1))
      case _ => None
    }
  }

  def bigram(fileItr: Iterator[String]): Iterator[List[String]] = {
    fileItr.flatMap { x => x.trim.split(" ").toList.sliding(2).toList }
  }


  def updateMap(gram: Iterator[List[String]]): Map[(String, String), Int] = {
    val hm = new mutable.HashMap[(String, String), Int]()
    gram.foreach { x =>
      val tup2 = stringToTuple2(x.mkString(" "))
      tup2 match {
        case Some(x) =>
          val freq = hm.get(x)
          freq match {
            case Some(_) => hm.update(x, hm(x) + 1)
            case _     => hm.put(x, 1)
          }
        case _ =>
      }
    }
    hm.toMap
  }

  def topN(top: Int)(gramMap: Map[(String, String), Int]): List[((String, String), Int)] = {
    gramMap.toList.sortWith(_._2 > _._2).take(top)
  }

  def search(firstTerm: String, secondTerm: String)(mp: Map[(String, String), Int]): 
            Map[(String, String),Int] = {
    mp.filter {
      case ((f, s), i) =>
        f == firstTerm && s == secondTerm
    }
  }

  def main(args: Array[String]): Unit = {
    def hamletLines = readFile("hamlet.txt", 381)
    
    val s = search("in", "the") _ compose updateMap _ compose bigram _
    println(s(hamletLines))
    
    val l = topN(10) _ compose updateMap _ compose bigram _
    println(l(hamletLines))
  }
}