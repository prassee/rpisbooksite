import scalaz._
import Scalaz._

object ScalazExample {

  def addOne(ls: List[Int]) = {
    Functor[List].map(ls) { _ + 1 }
  }
  
  addOne((1 to 10).toList) 

  Functor[Option].map(Some(10)){_ + 1}
  
  Functor[List].map(List("hello", "world", "howdy")) { _ + "!"} 
  
  List(1,2,4) <*> List({(_:Int) * 3}) 
  
  {(_: Int) * 3 }.point[List] 
  
  {(_: Int) * 3 }.pure[List]
  
  List(1,2,4) <*> List({(_:Int) * 3}) <*> List ({(_:Int) * 4})
  
  val func1 = List({ (_: Int) * 3 })
  
  val func2 = List({ (_: Int) * 4 })
  
  List(1,2,4) <*> List({(_:Int) * 3}, {(_:Int) * 4})
  
  Monad[List].map(List(1,2,3)){ x => List({ x * 3 }) }
}
