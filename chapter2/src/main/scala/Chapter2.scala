import scala.collection.parallel.immutable.ParSeq

object chapter2 {

  def isPrime(x: Int): Boolean = {
    (2 to Math.sqrt(x).toInt).forall(p => x % p != 0)
  }

  def countPrimesPure(lim: ParSeq[Int]) = {
    lim.filter(isPrime).length
  }

  def countPrimesImpure(lim: Int) = {
    var count = 0
    val l = (1 to lim).par
    l.map(x => if (isPrime(x)) count = count + 1)
    count
  }

  val s = countPrimesPure(Stream.from(1)
    .takeWhile(p => p < 1000000).par)

  def main(args: Array[String]): Unit = {
    val x = "Hello, World"

    val r1 = x.reverse
    val r2 = x.reverse

    val rr1 = "Hello, World".reverse
    val rr2 = "Hello, World".reverse

    val strBldr = new StringBuilder("Hello")
    val stringAppend1 = strBldr.append(", World").toString
    val stringAppend2 = strBldr.append(", World").toString

    //List
    val ls = List("hello", "world", "howdy")

    val xs = List("hello", "world", 11)

    val ints = List(1, 2, 3, 4)

    val ns = ls :+ "welcome"

    //Map
    val mp = Map("John" -> 53, "Alex" -> 22, "Smith" -> 37)

    val personDetails = Map("John" -> (53, "USA"), "Alex" -> (22, "Russia"), "Smith" -> (37, "UK"))

    //Set
    val ss = Set(1, 2, 3, 3)

    val mylist = ls :+ "howdy"

    mylist.toSet

    //List Transformations

    //map
    val myInts = List(1, 2, 3, 4)
    myInts.map { x => x + 1 }

    val myStrings = List("hello", "world", "howdy")
    myStrings.map { x => if (x == "world") { x + "!" } else x }

    //flatMap
    //val myStrings = List("hello", "world", "howdy")
    myStrings.flatMap { x => List(x + "!") }

    //filter
    //val ls = List("hello", "world", "howdy")
    ls.filter { x => x.startsWith("h") }

    val nums = (0 to 100).toList
    nums.filter { x => x % 3 == 0 }

    nums.filter { x => x % 3 == 0 }.filter { y => y % 5 == 0 }

    //sort

    val unsorted = List(3, 6, 8, 2, 9)
    unsorted.sorted

    //different sorting functions
    case class Employee(name: String, age: Int)
    val emps = List(Employee("John", 22), Employee("Alex", 53), Employee("Smith", 37))
    emps.sortBy(_.age)
    emps.sortBy(_.name)
    emps.sortWith(_.age > _.age)

    //foldLeft
    //val ls = List("hello", "world", "howdy")
    ls.foldLeft("")((a, b) => a + b)

    //foldRight
    ls.foldRight("")((a, b) => a + b)

    //Lazy val
    object LazyValDemo {
      lazy val z = { println("Initialising demo"); 100 }
    }

    LazyValDemo
    LazyValDemo.z

    object NotLazyValDemo {
      val z = { println("Initialising demo"); 100 }
    }

    NotLazyValDemo
    NotLazyValDemo.z

    object AreaOfCircle {
      lazy val pi = { println("pi"); 3.14 }
      lazy val r = { println("r"); 5 }
      lazy val area = { println("area"); pi * r * r }

      def calculate() = area
    }

    AreaOfCircle
    AreaOfCircle.calculate

    //Function composition
    object funcompose {
      def lift(ls: List[Int]) = ls map { x => Option(x) }
      def findNElem(elem: Int, ls: List[Option[Int]]): Boolean = ls match {
        case Nil     => false
        case x :: xs => if (elem == x.get) true else findNElem(elem, xs)
      }
    }

    def lift(ls: List[Int]) = ls map { x => Option(x) }

    def findNElem(elem: Int, ls: List[Option[Int]]): Boolean = ls match {
      case Nil     => false
      case x :: xs => if (elem == x.get) true else findNElem(elem, xs)
    }
    
    val findElemCurried = (funcompose.findNElem _).curried
    val fcompl = findElemCurried(3) compose funcompose.lift _
    fcompl(List(1,2,3))

  }

}