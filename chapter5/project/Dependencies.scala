import sbt._
import sbt.Keys._

object Dependencies {

  lazy val akkaVer = "2.3.12"

  lazy val chapter5Dep = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVer,
    "com.tumblr" %% "colossus" % "0.6.3",
    "org.java-websocket" % "Java-WebSocket" % "1.3.0",
    "com.typesafe.akka" %% "akka-slf4j" % akkaVer,
    "ch.qos.logback" % "logback-classic" % "1.0.13",
    "com.typesafe.akka" %% "akka-testkit" % "2.3.9",
    "org.scalatest" %% "scalatest" % "2.2.4" % "test",
    "commons-io" % "commons-io" % "2.4" % "test")
}
