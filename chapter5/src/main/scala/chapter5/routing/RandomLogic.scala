package chapter5.routing

import akka.actor.{Actor, ActorSystem, Props, Terminated}
import akka.routing.{ActorRefRoutee, RandomRoutingLogic, Router}

/**
  */
class RandomRoutingLogicActor extends Actor {

  val routeeCount = Vector.fill(5) {
    val r = context.actorOf(Props[WorkerActor])
    context watch r
    ActorRefRoutee(r)
  }

  private val randomRouter = Router(RandomRoutingLogic(), routeeCount)

  def receive = {
    case Work => randomRouter.route(Work, sender())
    case Report => routeeCount.foreach(af => af.send(Report, sender()))
    case Terminated(a) => randomRouter.removeRoutee(a)
  }
}

object RandomRoutingActor extends App {
  val as = ActorSystem("RoutingActorSystem")
  val actorRef = as.actorOf(Props[RandomRoutingLogicActor], "routingActor")
  for (i <- 0 until 10) {
    actorRef ! Work
    Thread.sleep(1000)
  }
  actorRef ! Report
}

