package chapter5.routing

import akka.actor.{Actor, ActorSystem, Props, Terminated}
import akka.routing.{ActorRefRoutee, Router, SmallestMailboxRoutingLogic}

/**
 * 
 */
class SmallestMailBoxRoutingActor extends Actor {

  val routees = Vector.fill(5) {
    val r = context.actorOf(Props[SlowWorker])
    context watch r
    ActorRefRoutee(r)
  }

  var smRouter = Router(SmallestMailboxRoutingLogic(), routees)

  def receive = {
    case Work => smRouter.route(Work, sender())
    case Report => routees.foreach(af => af.send(Report, sender()))
    case Terminated(a) => smRouter.removeRoutee(a)
  }
}

/**
 */
object SmallestMailbox extends App {

  val as = ActorSystem("RoutingActorSystem")
  val actorRef = as.actorOf(Props[SmallestMailBoxRoutingActor], "routingActor")

  for (i <- 0 until 10)
    actorRef ! Work

  actorRef ! Report
}

