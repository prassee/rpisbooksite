package chapter5.routing

import akka.actor.Actor

case object Work

case object Done

case object Report

class WorkerActor extends Actor {
  var msgCount = 0
  val actName = self.path.name

  override def preStart() {
    println(s"Starting actor ${actName}")
  }

  def receive = {
    case Work =>
      msgCount += 1
      println(actName + " -> " + msgCount)

    case Report =>
      println(s"total msgs received by ${actName} " + msgCount)
  }
}

class SlowWorker extends Actor {
  val actName = self.path.name
  var msgCount = 0

  def receive = {
    case Work =>
      msgCount += 1
      println(actName + " -> " + msgCount)

    case Report =>
      println(s"total msgs received by ${actName} " + msgCount)
  }

  override def preStart() = {
    println(s"starting slow actor ${actName}")
  }
}
