package chapter5.routing

import akka.actor.{Actor, ActorSystem, Props, Terminated}
import akka.routing.{ActorRefRoutee, RoundRobinRoutingLogic, Router}

class RoundRobinRoutingLogicActor extends Actor {

  val routees = Vector.fill(5) {
    val r = context.actorOf(Props[WorkerActor])
    context watch r
    ActorRefRoutee(r)
  }
  
  var rrRouter = Router(RoundRobinRoutingLogic(), routees)

  def receive = {
    case Work => rrRouter.route(Work, sender())
    case Report => routees.foreach(af => af.send(Report, sender()))
    case Terminated(a) => rrRouter.removeRoutee(a)
  }
}

/**
 *
 */
object Main extends App {

  val as = ActorSystem("RoutingActorSystem")
  val actorRef = as.actorOf(Props[RoundRobinRoutingLogicActor], "round-robin-routing-actor")
  for (i <- 0 until 10) {
    actorRef ! Work
    Thread.sleep(1000)
  }

  actorRef ! Report

}

