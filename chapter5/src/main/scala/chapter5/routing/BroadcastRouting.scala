package chapter5.routing

import akka.actor.{Actor, ActorSystem, Props, Terminated}
import akka.routing.{ActorRefRoutee, BroadcastRoutingLogic, Router}

class BroadcastRoutingActor extends Actor {

  val routees = Vector.fill(5) {
    val r = context.actorOf(Props[WorkerActor])
    context watch r
    ActorRefRoutee(r)
  }

  var bcRouter =
    Router(BroadcastRoutingLogic(), routees)

  def receive = {
    case Work => bcRouter.route(Work, sender())
    case Report => routees.foreach(af => af.send(Report, sender()))
    case Terminated(a) => bcRouter.removeRoutee(a)
  }
}

/**
  */
object BroadcastMailBoxRoutingApp extends App {
  val as = ActorSystem("RoutingActorSystem")
  val actorRef = as.actorOf(Props[BroadcastRoutingActor], "routingActor")

  for (i <- 0 until 10) {
    actorRef ! Work
    Thread.sleep(1000)
  }

  actorRef ! Report
}

