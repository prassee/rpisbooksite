package chapter5.routing

import java.util.concurrent.TimeUnit

import akka.actor.{ Actor, ActorSystem, Props, Terminated }
import akka.routing.{ ActorRefRoutee, Router, ScatterGatherFirstCompletedRoutingLogic }
import scala.concurrent.duration.Duration

/**
 *
 */
class ScatterGatherFirstCompletedRouting extends Actor {

  val routees = Vector.fill(5) {
    val r = context.actorOf(Props[WorkerActor])
    context watch r
    ActorRefRoutee(r)
  }

  var sgRouter =
    Router(ScatterGatherFirstCompletedRoutingLogic(Duration(1, TimeUnit.SECONDS)), routees)

  def receive = {
    case Work => sgRouter.route(Work, sender())
    case Report => routees.foreach(af => af.send(Report, sender()))
    case Terminated(a) => sgRouter.removeRoutee(a)
  }
}

/**
 */
object ScatterGatherFirstCompletedRouting extends App {
  val as = ActorSystem("RoutingActorSystem")
  val actorRef = as.actorOf(Props[ScatterGatherFirstCompletedRouting], "routingActor")

  for (i <- 0 until 10) {
    actorRef ! Work
    Thread.sleep(1000)
  }

  actorRef ! Report
}

