package chapter5

import akka.actor._
import scala.concurrent.duration._

object Start {

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("Master")

    val chefMaster = system.actorOf(ChefMaster.props(10.seconds), "ChefMaster")
    val ovenMaster = system.actorOf(OvenMaster.props(10.seconds), "OvenMaster")
    val deliveryMaster = system.actorOf(DeliveryMaster.props(10.seconds), "DeliveryMaster")

    chefMaster ! Work("1", "Margarita")
    chefMaster ! Work("2", "Marinara")
    chefMaster ! Work("3", "Napoletana")

    val chefWorker1 = system.actorOf(ChefWorker.props(chefMaster, ovenMaster), "Chefworker1")
    val chefWorker2 = system.actorOf(ChefWorker.props(chefMaster, ovenMaster), "Chefworker2")

    val ovenWorker1 = system.actorOf(OvenWorker.props(ovenMaster, deliveryMaster), "Ovenworker1")
    val ovenWorker2 = system.actorOf(OvenWorker.props(ovenMaster, deliveryMaster), "Ovenworker2")

    val deliveryWorker1 = system.actorOf(DeliveryWorker.props(deliveryMaster), "Deliveryworker1")

  }
}
