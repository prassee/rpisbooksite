package chapter5

case class Work(workId: String, job: Any)

case class WorkResult(workId: String, result: Any)
