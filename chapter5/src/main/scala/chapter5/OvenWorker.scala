package chapter5

import java.util.UUID
import scala.concurrent.duration._
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.ReceiveTimeout
import akka.actor.Terminated
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy.Stop
import akka.actor.SupervisorStrategy.Restart
import akka.actor.ActorInitializationException
import akka.actor.DeathPactException

object OvenWorker {

  def props(master: ActorRef, deliveryMaster: ActorRef): Props =
    Props(classOf[OvenWorker], master, deliveryMaster)

  case class WorkComplete(result: Any)
}

class OvenWorker(master: ActorRef, deliveryMaster: ActorRef) extends Actor with ActorLogging {
  import OvenWorker._
  import MasterWorkerProtocol._

  val workerId = UUID.randomUUID().toString

  override def preStart = master ! RegisterWorker(workerId)

  //val workExecutor = context.watch(context.actorOf(workExecutorProps, "exec"))

  var currentWorkId: Option[String] = None
  def workId: String = currentWorkId match {
    case Some(workId) => workId
    case None         => throw new IllegalStateException("Not working")
  }

  override def supervisorStrategy = OneForOneStrategy() {
    case _: ActorInitializationException => Stop
    case _: DeathPactException           => Stop
    case _: Exception =>
      currentWorkId foreach { workId => sendToMaster(WorkFailed(workerId, workId)) }
      context.become(idle)
      Restart
  }


  def receive = idle

  def idle: Receive = {
    case WorkIsReady =>
      master ! WorkerRequestsWork(workerId)

    case Work(workId, job) =>
      log.info("Got work: {}", job)
      currentWorkId = Some(workId)
      master ! WorkIsDone(workerId, workId, "")
      deliveryMaster ! Work(workId, job)
      context.become(waitForWorkIsDoneAck(""))
  }

  def working: Receive = {
    case WorkComplete(result) =>
      log.info("Work is complete. Result {}.", result)
      master ! WorkIsDone(workerId, workId, result)
      context.setReceiveTimeout(20.seconds)
      context.become(waitForWorkIsDoneAck(result))

    case _: Work =>
      log.info("Yikes. Master told me to do work, while I'm working.")
  }

  def waitForWorkIsDoneAck(result: Any): Receive = {
    case Ack(id) if id == workId =>
      master ! WorkerRequestsWork(workerId)
      context.setReceiveTimeout(Duration.Undefined)
      context.become(idle)
    case ReceiveTimeout =>
      log.info("No ack from master, retrying")
      master ! WorkIsDone(workerId, workId, result)
  }

  override def unhandled(message: Any): Unit = message match {
    //case Terminated(`workExecutor`) => context.stop(self)
    case WorkIsReady                => 
    case _                          => super.unhandled(message)
  }

  def sendToMaster(msg: Any): Unit = {
    //clusterClient ! SendToAll("/user/master/active", msg)
    master ! msg
  }

}
