package chapter5

import scala.collection.mutable
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import scala.concurrent.duration.Deadline
import scala.concurrent.duration.FiniteDuration
import akka.actor.Props

object ChefMaster {  

  def props(workTimeout: FiniteDuration): Props =
    Props(classOf[ChefMaster], workTimeout)

  case class Ack(workId: String)

  private sealed trait WorkerStatus
  private case object Idle extends WorkerStatus
  private case class Busy(workId: String, deadline: Deadline) extends WorkerStatus
  private case class WorkerState(ref: ActorRef, status: WorkerStatus)

  private case object CleanupTick

}

class ChefMaster(workTimeout: FiniteDuration) extends Actor with ActorLogging {
  import ChefMaster._
  
  val pendingWork =  mutable.Queue[Work]()
    
  private var workers = Map[String, WorkerState]()

  //private var workState = WorkState.empty

  import context.dispatcher
  val cleanupTask = context.system.scheduler.schedule(workTimeout / 2, workTimeout / 2,
    self, CleanupTick)

  override def postStop(): Unit = cleanupTask.cancel()

  def receive = {
    case MasterWorkerProtocol.RegisterWorker(workerId) =>
      if (workers.contains(workerId)) {
        workers += (workerId -> workers(workerId).copy(ref = sender()))
      } else {
        log.info("Chef worker registered: {}", workerId)
        workers += (workerId -> WorkerState(sender(), status = Idle))
        if (pendingWork.length > 0){
          sender() ! MasterWorkerProtocol.WorkIsReady
        }
      }

    case MasterWorkerProtocol.WorkerRequestsWork(workerId) =>
      if (pendingWork.length > 0) {
        workers.get(workerId) match {
          case Some(s @ WorkerState(_, Idle)) =>
            val work = pendingWork.dequeue 
              log.info("Giving chefworker {} some work {}", workerId, work.workId)
              workers += (workerId -> s.copy(status = Busy(work.workId, Deadline.now + workTimeout)))
              sender() ! work
          case _ =>
        }
      }

    case MasterWorkerProtocol.WorkIsDone(workerId, workId, result) =>
        log.info("Work {} is done by chefworker {}", workId, workerId)
        changeWorkerToIdle(workerId, workId)
        sender ! MasterWorkerProtocol.Ack(workId)

    case work: Work =>
        log.info("Chef master accepted work: {}", work.workId)
        pendingWork enqueue work
        notifyWorkers()
      
    case CleanupTick =>
      for ((workerId, s @ WorkerState(_, Busy(workId, timeout))) ← workers) {
        if (timeout.isOverdue) {
          log.info("Work timed out: {}", workId)
          workers -= workerId
          notifyWorkers()
        }
      }
  }

  def notifyWorkers(): Unit =
    if (pendingWork.length > 0) {
      // could pick a few random instead of all
      workers.foreach {
        case (_, WorkerState(ref, Idle)) => ref ! MasterWorkerProtocol.WorkIsReady
        case _                           => // busy
      }
    }

  def changeWorkerToIdle(workerId: String, workId: String): Unit =
    workers.get(workerId) match {
      case Some(s @ WorkerState(_, Busy(`workId`, _))) ⇒
        workers += (workerId -> s.copy(status = Idle))
      case _ ⇒
    }

}
