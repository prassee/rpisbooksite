package chapter5.buspub

/**
 *
 */
object BusbupBootStrap extends App {
  println("starting Queue")
  val as = akka.actor.ActorSystem("Busbup")
  val queueActor = as.actorOf(akka.actor.Props[VMQueueActor], "QueueActor")
  queueActor ! Publish("stomp")
  queueActor ! PublishMessage("stomp", "topic message 1")
  queueActor ! PublishMessage("stomp", "topic message 2")
}