package chapter5.buspub

import java.net.InetSocketAddress
import org.java_websocket.WebSocket
import org.java_websocket.handshake.ClientHandshake
import org.java_websocket.server.WebSocketServer

/**
 * Socket
 */
class BusbupSocket(port: Int) extends WebSocketServer(new InetSocketAddress(port)) {

  def onOpen(x$1: WebSocket, x$2: ClientHandshake) {
  }

  def onError(x$1: WebSocket, x$2: Exception) {
  }

  def onMessage(x$1: WebSocket, x$2: String) {
  }

  def onClose(x$1: WebSocket, x$2: Int, x$3: String, x$4: Boolean) {
  }
}
