package chapter5.buspub

case class Publish(name: String)
case class PublishMessage(topic: String, message: String)

/**
 *
 */
class VMQueueActor extends akka.actor.Actor {

  val topics = scala.collection.mutable.ListBuffer[String]()
  val topicMessages = scala.collection.mutable.Map[String, scala.collection.mutable.ListBuffer[String]]()

  def receive = {
    case topic: Publish => {
      if (!topics.contains(topic.name))
        topics += topic.name
      println("created topics " + topics)
    }

    case publish: PublishMessage => {
      if (topicMessages.get(publish.topic).isEmpty) {
        topicMessages += (publish.topic -> scala.collection.mutable.ListBuffer[String](publish.message))
      } else {
        topicMessages.get(publish.topic).get += publish.message
      }
      println("total messages on the queue " + topicMessages)
    }
  }

}

