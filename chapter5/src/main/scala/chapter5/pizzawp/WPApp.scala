package chapter5.pizzawp

import akka.actor.ActorSystem
import akka.actor.Props

/**
 *
 */
object WPApp {
  private lazy val wpActorSystem = ActorSystem("wp")
  private lazy val master = wpActorSystem.actorOf(Props[WPMaster])

  def createSlaves(x: Int) = for (z <- 1 to x) addSlave(z)

  private def addSlave(x: Int) = {
    val slave = wpActorSystem.actorOf(Props(classOf[WPSlave], master), name = "slave" + x)
  }

  def sendWorkMessage = {
    master ! WorkMessage(s"""some work message ${System.currentTimeMillis()} """)
  }
}

/**
 * Main program
 */
object WPMain extends App {
  val app = WPApp
  app.createSlaves(10)
  for (x <- 1 to 100) {
    app.sendWorkMessage
  }
}
