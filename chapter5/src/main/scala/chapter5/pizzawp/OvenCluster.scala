package chapter5.pizzawp

import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.actorRef2Scala
import chapter5.pizzawf.Dispatch
import chapter5.pizzawf.DoOrderRequest
import chapter5.pizzawf.FetchStove
import chapter5.pizzawf.Order

/**
 * @author prassee
 */
class OvenCluster(next: ActorRef)
    extends Actor
    with WPShared {

  def receive = {
    case fs: FetchStove =>
      orderQueue.enqueue(fs.orderRequest)
      if (!idleSlaves.isEmpty) {
        val slave = idleSlaves.dequeue()
        slave ! DoOrderRequest(next, orderQueue.dequeue())
      }

    case rs: RegsiterSlave =>
      slaves.+=(rs.slave)
      rs.slave ! Ack

    case AssignWork =>
      Try(orderQueue.dequeue()) match {
        case Success(e) => sender() ! DoOrderRequest(next, e)
        case Failure(e) => idleSlaves.enqueue(sender())
      }

    case WorkDone => self.forward(AssignWork)
  }
}

class OvenSlave(master: ActorRef) extends Actor {

  def receive = {
    case Ack => master ! AssignWork

    case or: DoOrderRequest =>
      or.next ! Dispatch(Order(or.or.noOfPizzas))
      master ! WorkDone
  }

  override def preStart() = {
    master ! RegsiterSlave(self)
  }
}