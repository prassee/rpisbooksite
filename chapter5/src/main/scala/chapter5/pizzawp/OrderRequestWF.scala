package chapter5.pizzawp

import scala.util.Failure
import scala.util.Success
import scala.util.Try
import akka.actor.Actor
import akka.actor.ActorRef
import chapter5.pizzawf.DoOrderRequest
import chapter5.pizzawf.OrderRequest
import chapter5.pizzawf.DoOrderRequest
import chapter5.pizzawf.PizzaPrepareRequest

/**
 * Master
 */
class OrderCluster(next: ActorRef)
    extends Actor
    with WPShared {

  def receive = {
    case or: OrderRequest =>
      orderQueue.enqueue(or)
      if (!idleSlaves.isEmpty) {
        val slave = idleSlaves.dequeue()
        slave ! DoOrderRequest(next, orderQueue.dequeue())
      }

    case rs: RegsiterSlave => {
      slaves.+=(rs.slave)
      rs.slave ! Ack
    }

    case AssignWork =>
      Try(orderQueue.dequeue()) match {
        case Success(e) => sender() ! DoOrderRequest(next, e)
        case Failure(e) => idleSlaves.enqueue(sender())
      }

    case WorkDone => self.forward(AssignWork)
  }
}

/**
 * Slave
 */
class OrderWorker(master: ActorRef)
    extends Actor {

  def receive = {
    case Ack => master ! AssignWork

    case or: DoOrderRequest =>
      println("received do order requeest")
      or.next ! PizzaPrepareRequest(or.or)
      // send to next master 
      master ! WorkDone
  }

  override def preStart() = {
    master ! RegsiterSlave(self)
  }
}

