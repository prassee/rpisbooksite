package chapter5.pizzawp

import akka.actor.Actor
import akka.actor.ActorRef
import chapter5.pizzawf.OrderRequest
import chapter5.pizzawf.PizzaPrepareRequest
import chapter5.pizzawf.DoOrderRequest
import scala.util.Try
import scala.util.Failure
import scala.util.Success
import chapter5.pizzawf.FetchStove

/**
 * @author prassee
 */
class ChefCluster(next: ActorRef)
    extends Actor
    with WPShared {

  def receive = {
    case or: PizzaPrepareRequest =>
      orderQueue.enqueue(or.orderRequest)
      if (!idleSlaves.isEmpty) {
        val slave = idleSlaves.dequeue()
        slave ! DoOrderRequest(next, orderQueue.dequeue())
      }

    case rs: RegsiterSlave =>
      slaves.+=(rs.slave)
      rs.slave ! Ack

    case AssignWork =>
      Try(orderQueue.dequeue()) match {
        case Success(e) => sender() ! DoOrderRequest(next, e)
        case Failure(e) => idleSlaves.enqueue(sender())
      }

    case WorkDone => self.forward(AssignWork)
  }
}

class ChefSlave(master: ActorRef) extends Actor {

  def receive = {
    case Ack => master ! AssignWork

    case or: DoOrderRequest =>
      or.next ! FetchStove(or.or)
      // send to next master 
      master ! WorkDone
  }

  override def preStart() = {
    master ! RegsiterSlave(self)
  }
}