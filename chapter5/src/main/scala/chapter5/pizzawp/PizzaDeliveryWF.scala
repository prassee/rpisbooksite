package chapter5.pizzawp

import akka.actor.ActorRef
import akka.actor.ActorSystem
import akka.actor.Props
import akka.actor.actorRef2Scala
import chapter5.pizzawf.OrderRequest

/**
 * WorkPulling based PizzaDelivery
 */
object PizzaDeliveryWF extends App {

  val pwpAs = ActorSystem("PizzaWorkPullActorSystem")

  lazy val deliveryMaster = pwpAs.actorOf(Props[OrderDelivery])

  import ClusterBootStrap._

  lazy val ovenMaster = pwpAs.actorOf(Props(classOf[OvenCluster], deliveryMaster), "OvenMaster")

  createSlaves(5, ovenMaster, pwpAs.actorOf(Props(classOf[OvenSlave], ovenMaster)))

  lazy val chefMaster = pwpAs.actorOf(Props(classOf[ChefCluster], ovenMaster), "ChefMaster")

  createSlaves(5, chefMaster, pwpAs.actorOf(Props(classOf[ChefSlave], chefMaster)))

  val orderMaster = pwpAs.actorOf(Props(classOf[OrderCluster], chefMaster), name = "OrderMaster")

  createSlaves(3, orderMaster, pwpAs.actorOf(Props(classOf[OrderWorker], orderMaster)))
  
  // first order - more orders to  come 
  orderMaster ! OrderRequest(3, "crunchy Pizza", "delivery address")

}

/**
 *
 */
object ClusterBootStrap {

  def createSlaves(slaves: Int, master: ActorRef, slave: ActorRef) = {
    for (x <- 1 to slaves) {
      master ! RegsiterSlave(slave)
    }
  }
}
