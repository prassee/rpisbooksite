package chapter5.pizzawp

import scala.util.Failure
import scala.util.Success
import scala.util.Try

import akka.actor.Actor
import akka.actor.ActorRef
import akka.actor.actorRef2Scala


/**
 * Master
 */
class WPMaster extends Actor with WPShared {

  def receive = {

    case wm: WorkMessage =>
      queue.enqueue(wm)

      if (!idleSlaves.isEmpty) {
        val slave = idleSlaves.dequeue()
        slave ! queue.dequeue()
      }

    case rs: RegsiterSlave =>
      slaves.+=(rs.slave)
      rs.slave ! Ack

    case AssignWork =>
      Try(queue.dequeue()) match {
        case Success(e) => sender().!(e)
        case Failure(e) => idleSlaves.enqueue(sender())
      }

    case WorkDone =>
      self.forward(AssignWork)
  }

}

/**
 * Slave
 */
class WPSlave(master: ActorRef) extends Actor {

  private lazy val random = new java.util.Random()

  def receive = {

    case Ack =>
      master ! AssignWork

    case wm: WorkMessage =>
      Thread.sleep(random.nextInt(9000))
      println(s"total msgs handled yet by slave ${self.path.name}")
      master ! WorkDone
  }

  override def preStart() = {
    master ! RegsiterSlave(self)
  }

}