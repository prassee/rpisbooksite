package chapter5.pizzawp

import akka.actor.ActorRef

case class RegsiterSlave(slave: ActorRef)
case object Ack
case object AssignWork
case class WorkMessage(a: String)
case object WorkDone
