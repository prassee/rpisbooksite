package chapter5.pizzawp

import scala.collection.mutable.ListBuffer
import chapter5.pizzawf.OrderRequest
import scala.collection.mutable.Queue
import akka.actor.ActorRef

trait WPShared {
  protected val slaves = ListBuffer[ActorRef]()
  protected val queue = Queue[WorkMessage]()
  protected val orderQueue = Queue[OrderRequest]()
  protected val idleSlaves = Queue[ActorRef]()
}