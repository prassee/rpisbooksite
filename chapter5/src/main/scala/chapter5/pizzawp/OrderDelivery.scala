package chapter5.pizzawp

import akka.actor.Actor
import chapter5.pizzawf.Dispatch

/**
 * @author prassee
 */
class OrderDelivery extends Actor with WPShared {

  def receive = {

    case x: Dispatch =>
      println("Pizza ready for delivery")
  }

}
