package chapter5.pizzawf

import akka.actor.Actor
import akka.routing.ActorRefRoutee
import akka.routing.Router

/**
 *
 */
abstract class AbstractRouter(route: RouterParam) extends Actor {

  var router: Router = null

  override def preStart() = {
    val routes = Vector.fill(route.workers) {
      val r = context.actorOf(route.props)
      context watch r
      ActorRefRoutee(r)
    }
    router = Router(route.routing, routes)
  }
}
