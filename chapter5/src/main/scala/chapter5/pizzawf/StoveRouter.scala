package chapter5.pizzawf

import akka.actor.Actor

class StoveRouter(param: RouterParam) extends AbstractRouter(param) {
  def receive = {
    case fs: FetchStove => router.route(fs, sender())
  }
}

class StoveWorker extends Actor {
  def receive = {
    case fs: FetchStove =>
      Thread.sleep(fs.orderRequest.noOfPizzas * 2000)
      sender ! Delivered
  }
}
