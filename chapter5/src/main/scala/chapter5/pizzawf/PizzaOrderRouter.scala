package chapter5.pizzawf

import akka.actor.Actor

/**
 *
 */
class PizzaOrderRouter(param: RouterParam) extends AbstractRouter(param) {
  def receive: PartialFunction[Any, Unit] = {
    case or: OrderRequest => router.route(or, sender())
  }
}

class PizzaOrderHandlingWorker extends Actor {
  def receive = {
    case or: OrderRequest => sender ! PizzaPrepareRequest(or)
  }
}
