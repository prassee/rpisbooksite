package chapter5.pizzawf

import akka.actor.Props
import akka.routing.RoutingLogic
import akka.actor.ActorRef

case class OrderRequest(noOfPizzas: Int,
                        name: String,
                        deliveryAddress: String)
case class DoOrderRequest(next: ActorRef, or: OrderRequest)

case class PizzaPrepareRequest(orderRequest: OrderRequest)

case class Order(order: Int)
case class Dispatch(order: Order)
case class RouterParam(workers: Int,
                       props: Props,
                       routing: RoutingLogic)
case class FetchStove(orderRequest: OrderRequest)
case object Delivered