package chapter5.pizzawf

import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.routing._
import akka.actor.{Actor, ActorRef, ActorSystem, Props}
import akka.routing._
import scala.collection.mutable

object PizzaShopApp extends App {
  val as = ActorSystem("pizza-shop")
  val shop = as.actorOf(Props[PizzaShop])
  val orderReq = OrderRequest(3, "firstOrder", "some address")
  shop ! orderReq
}

/**
 *
 */
class PizzaShop extends Actor {

  private val flowHeads = mutable.Map[String, ActorRef]()

  def receive = {
    case or: OrderRequest =>
      println(s"received OrderRequest ${or.name}")
      flowHeads.get("orderReqOffice").get ! or

    case pr: PizzaPrepareRequest =>
      println(s"received PrepareRequest ${pr.orderRequest.name}")
      flowHeads.get("chefOffice").get ! pr

    case fs: FetchStove =>
      println(s"received FetchStove ${fs.orderRequest.name}")
      flowHeads.get("stoveOffice").get ! fs

    case Delivered =>
      println("Pizza ready for delivery")
  }

  override def preStart(): Unit = {

    def actorRef = (af: Actor) => context.actorOf(Props(af))

    flowHeads.+=(
      "orderReqOffice" -> actorRef(
        new PizzaOrderRouter(
          RouterParam(3, Props[PizzaOrderHandlingWorker], RandomRoutingLogic())
        )
      ),
      "chefOffice" -> actorRef(
        new PizzaChefRouter(
          RouterParam(3, Props[PizzaChefWorker], SmallestMailboxRoutingLogic())
        )
      ),
      "stoveOffice" -> actorRef(
        new StoveRouter(
          RouterParam(3, Props[StoveWorker], RoundRobinRoutingLogic())
        )
      )
    )
  }
}
