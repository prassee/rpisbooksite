package chapter5.pizzawf

import akka.actor.Actor

class PizzaChefRouter(param: RouterParam) extends AbstractRouter(param) {
  def receive = {
    case or: PizzaPrepareRequest => router.route(or.orderRequest, sender())
  }
}

class PizzaChefWorker extends Actor {
  def receive = {
    case or: OrderRequest => sender ! FetchStove(or)
  }
}
