libraryDependencies += "org.scalaz" %% "scalaz-core" % "7.1.1"

initialCommands in console := "import scalaz._, Scalaz._"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % "2.3.11"