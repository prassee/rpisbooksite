package chapter4

import akka.actor.{ Actor, ActorRef, Props, ActorSystem }
import akka.actor.OneForOneStrategy
import akka.actor.SupervisorStrategy._
import akka.actor.ActorLogging
import akka.actor.AllForOneStrategy
import akka.actor.OneForOneStrategy
import akka.actor.AllForOneStrategy
import akka.actor.Terminated
import PizzaMessages._

class Pasta extends Actor with ActorLogging {
  def receive = {
    case PastaBadRequest =>
      log.info("I received a pasta request")
      throw new PastaException("Pasta is stale!")
    case PastaGoodRequest =>
      log.info("Great pasta served!")
  }
}

class PizzaSupervisor1 extends Actor with ActorLogging {

  val pizzaToppings = context.actorOf(Props[PizzaToppings], "PizzaToppings")
  val pasta = context.actorOf(Props[Pasta], "Pasta")

  def receive = {
    case MarinaraRequestWithPasta =>
      log.info("I have a Marinara request with extra cheese!")
      pizzaToppings ! ExtraCheese
      pasta ! PastaBadRequest
      pasta ! PastaGoodRequest

    case MargheritaRequest =>
      log.info("I have a Margherita request!")
      pizzaToppings ! ToppingsBadRequest

    case PizzaException => throw new Exception("Pizza fried!")
  }

  override val supervisorStrategy = OneForOneStrategy() {
    case _: PastaException =>
      log.error(s"Restarting pasta actor")
      Stop

    case _: ToppingsException =>
      log.error("Restarting toppings actor")
      Restart
  }

}

object TestActorPath1 {

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("Pizza")
    val pizza: ActorRef = system.actorOf(Props[PizzaSupervisor], "PizzaSupervisor")
    pizza ! MarinaraRequestWithPasta

    pizza ! MargheritaRequest
  }

}