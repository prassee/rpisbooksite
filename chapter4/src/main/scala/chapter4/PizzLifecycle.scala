package chapter4

import akka.actor.{ Actor, ActorRef, Props, ActorSystem, ActorLogging }
import PizzaMessages._

class PizzaLifeCycle extends Actor with ActorLogging {

  override def preStart() = {
    log.info("Pizza request received!")
  }

  def receive = {
    case MarinaraRequest   => log.info("I have a Marinara request!")
    case MargheritaRequest => log.info("I have a Margherita request!")
    case PizzaException    => throw new Exception("Pizza fried!")
  }
  
  override def preRestart(reason: Throwable, message: Option[Any]) = {
    log.info("Pizza baking restarted because " + reason.getMessage)
    postStop()
  }
  
  
  override def postRestart(reason: Throwable) = {
    log.info("New Pizza process started because " + reason.getMessage)
    preStart()
  }

  override def postStop() = {
    log.info("Pizza request finished")
  }
  
}

object PizzaLifecycleApp {

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("Pizza")
    val pizza: ActorRef = system.actorOf(Props[PizzaLifeCycle], "PizzaLifeCycle")
    
    pizza ! MarinaraRequest
    pizza ! PizzaException
    pizza ! MargheritaRequest
    //system.shutdown()
  }


}