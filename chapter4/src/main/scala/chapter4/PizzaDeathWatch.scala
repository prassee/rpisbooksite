package chapter4

import akka.actor.{ Actor, ActorRef, Props, ActorSystem, ActorLogging }
import akka.actor.PoisonPill
import akka.actor.DeadLetter
import akka.actor.DeadLetter
import akka.actor.Terminated
import akka.actor.Kill
import PizzaMessages._

class PizzaActor extends Actor with ActorLogging {

  override def preStart() = {
    log.info("Pizza request received!")
  }

  def receive = {
    case MarinaraRequest   => log.info("I have a Marinara request!")
    case MargheritaRequest => log.info("I have a Margherita request!")
    case PizzaException    => throw new Exception("Pizza fried!")
    case StopPizzaBaking   => context.stop(self)
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    log.info("Pizza baking restarted because " + reason.getMessage)
    postStop()
  }

  override def postRestart(reason: Throwable) = {
    log.info("New Pizza process started because " + reason.getMessage)
    preStart()
  }

  override def postStop() = {
    log.info("Pizza request finished")
  }

}

class PizzaWatcher extends Actor with ActorLogging {

  val pizza = context.actorOf(Props[PizzaActor], "pizzaactor")
  context.watch(pizza)

  def receive = {

    case "Stop" =>
      pizza ! PoisonPill

    case Terminated(terminatedActorRef) =>
      log.error(s"Actor ${terminatedActorRef} terminated")
  }
}

object PizzaDeathWatch {

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("Pizza")
    val pizza = system.actorOf(Props[PizzaActor], "pizzaactor")

    val pizzaWatcher = system.actorOf(Props[PizzaWatcher], "pizzawatcher")

    pizza ! MarinaraRequest
    pizzaWatcher ! "Stop"
  }

}