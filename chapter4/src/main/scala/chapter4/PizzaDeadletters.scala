package chapter4

import akka.actor.{ Actor, ActorRef, Props, ActorSystem, ActorLogging }
import akka.actor.PoisonPill
import akka.actor.DeadLetter
import akka.actor.DeadLetter
import PizzaMessages._

class PizzaDeadletters extends Actor with ActorLogging {

  override def preStart() = {
    log.info("Pizza request received!")
  }

  def receive = {
    case MarinaraRequest   => log.info("I have a Marinara request!")
    case MargheritaRequest => log.info("I have a Margherita request!")
    case PizzaException    => throw new Exception("Pizza fried!")
    case StopPizzaBaking   => context.stop(self)
  }

  override def preRestart(reason: Throwable, message: Option[Any]) = {
    log.info("Pizza baking restarted because " + reason.getMessage)
    postStop()
  }

  override def postRestart(reason: Throwable) = {
    log.info("New Pizza process started because " + reason.getMessage)
    preStart()
  }

  override def postStop() = {
    log.info("Pizza request finished")
  }

}

class PizzaDeadLetterListener extends Actor with ActorLogging{
  
  def receive = {
    case deadletter: DeadLetter =>
      log.info(s"Unprocessed pizza request $deadletter")
  }
}

object PizzaDeadletterApp {

  def main(args: Array[String]): Unit = {

    val system = ActorSystem("Pizza")
    val pizza: ActorRef = system.actorOf(Props[PizzaDeadletters], "PizzaDeadletters")

    val pizzaDeadletters = system.actorOf(Props[PizzaDeadLetterListener])
    system.eventStream.subscribe(pizzaDeadletters, classOf[DeadLetter])
    
    pizza ! MarinaraRequest
    pizza ! StopPizzaBaking
    pizza ! MargheritaRequest
  }

}