package chapter4

import akka.actor.{ Actor, ActorRef, Props, ActorSystem, ActorLogging }

import PizzaMessages._

class PizzaToppings extends Actor{
  def receive = {
    case ExtraCheese => println("Aye! Extra cheese it is")
    case Jalapeno => println("More Jalapenos!")
  }
}

class PizzaSupervisor extends Actor {
  
  val pizzaToppings = 
        context.actorOf(Props[PizzaToppings], "PizzaToppings")
  
  def receive = {
    case MarinaraRequest   => 
      println("I have a Marinara request with extra cheese!")
      println(pizzaToppings.path)
      pizzaToppings ! ExtraCheese
      
    case MargheritaRequest => 
      println("I have a Margherita request!")
    case PizzaException    => 
     throw new Exception("Pizza fried!")
  }
}

object TestActorPath {
  
  def main(args: Array[String]): Unit = {

    val system = ActorSystem("Pizza")
    val pizza: ActorRef =  system.actorOf(Props[PizzaSupervisor], 
                   "PizzaSupervisor")
    println(pizza.path)
    pizza ! MarinaraRequest
    system.shutdown()
  }
}