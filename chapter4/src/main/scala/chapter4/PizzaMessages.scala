package chapter4

object PizzaMessages {
  case object Jalapeno extends PizzaRequest
  case object ExtraCheese extends PizzaRequest
  case object MarinaraRequest extends PizzaRequest
  case object StopPizzaBaking extends PizzaRequest
  case object PizzaException extends Exception
}