package chapter8

case class OrderAck(orderId: Int, name: String, list: List[OrderItem])
case class OrderItem(item: Item, quantity: Int)
case object CheezyVegPizza extends Pizza

trait Item {
  def getPrice: Double
}

trait Pizza extends Item {
  override def getPrice = 200
}

trait Drink extends Item {
  override def getPrice = 80
  def doStuff = {
    val x = (a: Int) ⇒ a * 2
    println(x(20))
  }
}

case object HotChocolate extends Drink {
  override def getPrice = super.getPrice + 70
}

case object Lemonade extends Drink
case class Crust(thick: Int, radius: Int)
case object FruitMocktail extends Drink {
  override def getPrice = super.getPrice + 30
}

case class OrderNotFulfilledException(a: String) extends Exception(a)

object OrderDesk {
  import scala.collection.mutable.ListBuffer
  val order = ListBuffer[OrderItem]()

  var orderNumber = 0

  def addItem(item: OrderItem) = {
    order += item
    this
  }

  def getOrder = order

  def done(name: String) = {
    if (getOrder.isEmpty) {
      throw OrderNotFulfilledException("No order placed yet")
    }
    orderNumber += 1
    val ordered = OrderAck(orderNumber, name, order.toList)
    order.clear()
    ordered
  }

  def orderTotal =
    getOrder.map(x => x.item.getPrice * x.quantity).sum
}
