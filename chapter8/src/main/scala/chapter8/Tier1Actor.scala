package chapter8

import akka.actor.Actor
import akka.actor.ActorRef

class DoStuffActor extends Actor {
  def receive = {
    case ds: DoStuff => sender ! ds.a
      //t2Actor.!(DonStuff(ds.a))
  }
}

class DonStuffActor extends Actor {
  def receive = {
    case dns: DonStuff => println(dns.a)
  }
}

case class DoStuff(a: String)
case class DonStuff(a: String)
