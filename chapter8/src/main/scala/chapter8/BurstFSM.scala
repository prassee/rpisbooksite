package chapter8

import chapter7.{
  BurstAccept,
  BurstData,
  BurstEnd,
  BurstStart,
  CalcBurstValues,
  Init,
  Messages,
  SessionEnd,
  SessionStart,
  State
}

class BurstFSM[S <: State] {

  protected var currState: State = Init

  private def goto(state: State) = {
    currState = state
    currState
  }

  private def stay = currState

  def handle(messages: Messages): Unit = {
    currState match {
      case Init => initParFun(messages)
      case BurstAccept => burstAcceptFun(messages)
      case CalcBurstValues => calcBurstValues(messages)
    }
  }

  private def initParFun: PartialFunction[Messages, State] = {
    case SessionStart => {
      goto(BurstAccept)
    }
    case _ => stay
  }

  private def burstAcceptFun: PartialFunction[Messages, State] = {
    case BurstStart => goto(CalcBurstValues)
    case _ => stay
  }

  private def calcBurstValues: PartialFunction[Messages, State] = {
    case a: BurstData => stay
    case BurstEnd => goto(BurstAccept)
    case SessionEnd => goto(Init)
    case _ => stay
  }

}
