package chapter8

import org.scalatest._

class PizzaOrderTest extends FunSpec with Matchers with Fixtures {

  describe("PizzaOrder") {

    it("Has OrderItems") {
      orderbearer.getOrder should have size 2
    }

    it("Makes OrderTaker to place order") {
      val total = orderbearer.orderTotal
      total should not be notExpectedTotal
      total shouldBe expectedTotal
    }

    it("match all possible matchers") {
      val total = orderbearer.orderTotal
      val orderedItems = orderbearer.getOrder

      // value matchers
      total should not be notExpectedTotal
      total shouldBe expectedTotal

      // colleciton matchers
      orderedItems should contain(cheVegPizza)
      orderedItems should not contain (hotChoco)

      // reference matchers
      val newOrderTaker = orderbearer
      newOrderTaker should be theSameInstanceAs orderbearer

      // compund matchers
      orderedItems should (
        contain(cheVegPizza) and
        (not contain (hotChoco)) or
        contain(lemonade)
      )

      // property matchers
      val lemonde = orderedItems.apply(1)
      lemonde should have('item(Lemonade), 'quantity(2))
    }

    it("Done with order should ack the as OrderAck object") {
      orderbearer.order.isEmpty shouldBe (false)
      orderbearer.done(orderName)
    }

    it("throws an Exception when done with no items being ordered ") {
      // just assert if the exception is thrown
      intercept[OrderNotFulfilledException] {
        orderbearer.done("")
      }
      // assert the excetion message
      val expectException = evaluating {
        orderbearer.done("")
      } should produce[OrderNotFulfilledException]
      expectException.getMessage() should be(orderException.getMessage)
    }
  }
}

/**
 * WordSpec variation of FunSpec
 */
class PizzaOrderWordSpecTest extends WordSpec with Matchers with Fixtures {

  "A Pizza order" when {
    "placed" should {
      "exactly have the same no of items" in {
        orderbearer.getOrder should have size (2)
      }
    }
  }
}

/**
 *
 */
class FeatureSpectest extends FeatureSpec with Matchers with Fixtures {

  feature("A Pizza order when placed should exactly have the same no of items") {
    orderbearer.getOrder should have size (2)
  }

}

/**
 *
 */
class FlatSpecTest extends FlatSpec with Matchers with Fixtures {

  behavior of "A Pizza order "

  it should ("exactly have the same no of items") in {
    orderbearer.getOrder should have size (2)
  }

}

