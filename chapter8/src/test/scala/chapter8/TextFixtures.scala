package chapter8

/*
 test fixtures
 */
trait Fixtures {
  val cheVegPizza = OrderItem(CheezyVegPizza, 1)
  val lemonade = OrderItem(Lemonade, 2)
  val hotChoco = OrderItem(HotChocolate, 1)
  val orderbearer = OrderDesk
  val orderName = "my new order"
  val orderException = OrderNotFulfilledException("No order placed yet")
  orderbearer.addItem(cheVegPizza).addItem(lemonade)

  val expectedTotal = 360.00
  val notExpectedTotal = 0.00

  OrderDesk
    .addItem(OrderItem(CheezyVegPizza, 1))
    .addItem(OrderItem(Lemonade, 2))
}
