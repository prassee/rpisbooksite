package chapter8

import org.scalamock.scalatest.MockFactory
import org.scalatest.{ Matchers, WordSpecLike }

/**
 * ScalaMock Demo
 */
class ScalaMockDemo extends WordSpecLike with Matchers with MockFactory {

  val mScoreBoard = stub[Agent]

  "ScoreBoard" when {
    "Asked for Score" should {
      "give me a valid Score" in {
        (mScoreBoard.getData _) when () returns (Data("23"))
        val x = new DataCollector()
        val score = x.doStuff(mScoreBoard)
        val exp = mScoreBoard.getData
        score shouldBe (Integer.parseInt(exp.frame))
      }

      "give me a valid score" in {
        val x = 90
        x should not equal 23
      }
    }
  }
}

class DataCollector {
  def doStuff(agent: Agent) = {
    Integer.parseInt(agent.getData.frame)
  }
}

trait Agent {
  def getData = Data("23")
}

case class Data(frame: String)
