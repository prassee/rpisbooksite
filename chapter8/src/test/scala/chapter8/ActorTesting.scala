package chapter8

import akka.actor.ActorSystem
import akka.actor.Props
import akka.testkit._
import chapter7.{End, DataPoint, BecomeUnBecome}
import org.scalatest._

import scala.concurrent.duration._
import scala.language.postfixOps._


class TestKitUsageSpec
  extends TestKit(ActorSystem("TestKitUsageSpec"))
  with DefaultTimeout
  with ImplicitSender
  with FunSpecLike
  with Matchers {

  val ds = DoStuff("doStuff")

  describe("our actor testing") {
    val doStuffActor = system.actorOf(Props[DoStuffActor])
    val af = TestActorRef[BecomeUnBecome]

    it("takes a DoStuff message and return") {
      within(500 millis) {
        doStuffActor.!(ds)
        expectMsg(ds.a)
      }
    }

    it("assert the actor contain DataPoint being sent") {
      af ! DataPoint(2)
      af.underlyingActor.getNumbers should contain(2)
    }

  }

}

class BecomeUnBecomeActorSpec
  extends TestKit(ActorSystem("TestKitUsageSpec"))
  with DefaultTimeout
  with ImplicitSender
  with FunSpecLike
  with Matchers {


  describe("our actor testing") {
    val becomenUnBecomeActor = system.actorOf(Props[BecomeUnBecome])
    it("assert for reveiving DataPoint") {
      within(500 millis) {
        becomenUnBecomeActor ! DataPoint(10)
        /*becomenUnBecomeActor ! DataPoint(11)
        becomenUnBecomeActor ! DataPoint(12)
        becomenUnBecomeActor ! DataPoint(13)*/

        // expectMsg(DataPoint(10))
        expectNoMsg()
        /*expectMsg(DataPoint(11))
        expectMsg(DataPoint(12))
        expectMsg(DataPoint(13))*/
      }
    }
  }
}
