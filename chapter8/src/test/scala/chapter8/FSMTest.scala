package chapter8

import org.scalatest._
import chapter7.{
  BurstAccept,
  BurstData,
  BurstEnd,
  BurstStart,
  CalcBurstValues,
  Init,
  Messages,
  SessionEnd,
  SessionStart,
  State
}

/**
 * FSMTest
 */
class FSMTest extends FunSpec with Matchers {

  val fsm = new OurMockFSM()

  describe("PlainFSMClass") {

    it("Should be in Init State when created") {
      fsm.getState shouldBe (Init)
    }

    it("Should be in BurstAccept when sent SessionStart Message") {
      fsm.handle(SessionStart)
      info("this an inform demo")
      fsm.getState shouldBe (BurstAccept)
    }

    it("Should stay in the same State if any other messages is passed") {
      fsm.handle(BurstEnd)
      fsm.getState shouldBe (BurstAccept)
    }

    it("Should goto CalcBurstValues when sent BurstStart") {
      fsm.handle(BurstStart)
      fsm.getState shouldBe (CalcBurstValues)
      pending
    }

    it("Should stay for all BurstData messages") {
      fsm.handle(BurstData(List(("XYS" -> 23))))
      fsm.getState shouldBe (CalcBurstValues)
    }

    it("BurstEnd") {
      fsm.handle(BurstEnd)
      fsm.getState shouldBe (BurstAccept)
    }

    it("Again sent BurstStart Message") {
      fsm.handle(BurstStart)
      fsm.getState shouldBe (CalcBurstValues)
    }

    it("Move to Init") {
      val fs = "full-state"
      fsm.getFullState shouldBe fs
      fsm.handle(SessionEnd)
      fsm.handle(SessionEnd)
      fsm.getState shouldBe (Init)
    }
  }
}

class OurMockFSM extends BurstFSM[State] {
  def getState = Init
  def getFullState = "full-state"
}

