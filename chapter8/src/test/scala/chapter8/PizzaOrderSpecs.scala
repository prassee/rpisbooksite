package chapter8

import org.specs2.mutable._
import org.specs2.runner.JUnitRunner
import org.junit.runner.RunWith

/**
 * PizzaOrderSpecs test
 */
class PizzaOrderSpecs extends Specification with Fixtures {

  sequential ^ """Pizza shop bearer""" >> {

    """Has OrderItems""" >> {
      orderbearer.getOrder.size must be_==(2)
    }

    """Makes OrderTaker to place order""" >> {
      val total = orderbearer.orderTotal
      total must be_!=(0.0)
      total must be_==(360.00)
    }

    """Done with order should ack the as OrderAck object""" >> {
      orderbearer.getOrder.isEmpty must be_==(false)
    }
  }

  sequential ^ """Execute all possible """ >> {
    """all matchers""" >> {
      val total = orderbearer.orderTotal
      val orderedItems = orderbearer.getOrder

      // value matchers
      total must be_!=(0.0)
      total must be_==(360.00)

      // collection matchers
      orderedItems should contain(OrderItem(CheezyVegPizza, 1))
      orderedItems should not contain (OrderItem(HotChocolate, 1))

      // reference matchers
      val newOrderTaker = orderbearer
      newOrderTaker must beTheSameAs(orderbearer)

      // compound matchers
      orderedItems must contain(OrderItem(CheezyVegPizza, 1))
      orderedItems must not contain (OrderItem(HotChocolate, 1))
      orderedItems must contain(OrderItem(Lemonade, 2))

      // property matchers
      /*val lemonade = orderedItems.apply(1)
      lemonade must contain('item(Lemonade), 'quantity(2))*/
    }
  }

  sequential ^ """Pizza Shop bearer""" >> {

    """Assert for empty after done with order""" >> {
      orderbearer.done("")
      orderbearer.getOrder.isEmpty must be_==(true)
    }

    """Should throw an Exception when done with no items being ordered""" >> {
      orderbearer.done("order") must throwA[OrderNotFulfilledException]
    }

  }
}
