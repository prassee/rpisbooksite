import sbt._
import sbt.Keys._

object Dependencies {

  val akka=    "com.typesafe.akka" %% "akka-actor" % "2.3.11"
  val akkaTestKit =    "com.typesafe.akka" %% "akka-testkit" % "2.3.11"
  val specs =     "org.specs2" %% "specs2-core" % "3.6.4" % "test"
  val specsJunit =    "org.specs2" %% "specs2-junit" % "3.6.4" % "test"
  val specsMock =  "org.specs2" %% "specs2-mock" % "3.6.4" % "test"
  val scalaTest = "org.scalatest" % "scalatest_2.11" % "2.2.4" % "test"
  val scalaMock = "org.scalamock" %% "scalamock-scalatest-support" % "3.2" % "test"
  val junit =  "junit" % "junit" % "4.12"
  // other chapter code
  val chapter3 = "rpisbooksite" %% "chapter3" % "0.1.0"
  val chapter5 = "rpisbooksite" %% "chapter5" % "0.1.0"
  val chapter7 = "rpisbooksite" %% "chapter7" % "0.1.0"
  val chapter8Dep = Seq(akka,akkaTestKit,specs,specsJunit,specsMock,scalaTest,scalaMock,junit,chapter5,chapter3,chapter7)

}
