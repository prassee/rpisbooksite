package chapter7

import akka.actor.Actor
import akka.actor.FSM

// state - 
sealed trait FSMSignal
case object RedSignal extends FSMSignal
case object YellowSignal extends FSMSignal
case object GreenSignal extends FSMSignal

// data  
case class SignalColorData(str: String)

// event
case object ChangeSignal
case object RetainSignal

/**
 * Actor
 */
class SignalChangeFSMActor extends Actor with FSM[FSMSignal, SignalColorData] {

  // initial state of FSM
  startWith(RedSignal, SignalColorData("data"))

  when(RedSignal) {
    case Event(ChangeSignal, _) => goto(YellowSignal)
    case Event(RetainSignal, _) => stay
  }

  when(YellowSignal) {
    case Event(ChangeSignal, _) => goto(GreenSignal)
    case Event(RetainSignal, _) => stay
  }

  when(GreenSignal) {
    case Event(ChangeSignal, _) => goto(RedSignal)
    case Event(RetainSignal, _) => stay
  }

  onTransition {
    case RedSignal -> YellowSignal => println("Changing from red to yellow signal - get ready to go")
    case YellowSignal -> GreenSignal => println("Changing from yellow to green signal - wroooom")
    case GreenSignal -> RedSignal => println("Changing from green to red signal - stop")
  }

  initialize
}
