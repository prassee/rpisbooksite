package chapter7

import akka.actor.{Props, ActorSystem, Actor}

import scala.collection.mutable.ListBuffer

/**
 *
 */
class BecomeUnBecome extends Actor {
  private var numbers = ListBuffer[Int]()

  def otherReceive: Receive = {
    case Calc =>
      val x = numbers.sum / numbers.size
      println("Average Calculated => " + x)
      context.become(firstReceive)
  }

  def firstReceive: Receive = {
    case End => context.become(otherReceive)
    case DataPoint(a) => numbers.+=(a)
  }

  def getNumbers = numbers.toList

  def receive = firstReceive
}

case object End

case object Calc

case class DataPoint(a: Int)

object BecomeUnBecomeDemo extends App {
  println("starting")
  val as = ActorSystem("bub")
  val becomeUnbecomeActor = as.actorOf(Props[BecomeUnBecome], "BecomeUnBecome")

  becomeUnbecomeActor.!(DataPoint(10))
  becomeUnbecomeActor.!(DataPoint(11))
  becomeUnbecomeActor.!(DataPoint(12))
  becomeUnbecomeActor.!(DataPoint(13))
  becomeUnbecomeActor.!(End)
  becomeUnbecomeActor.!(Calc)

}
