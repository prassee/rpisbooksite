package chapter7

import scala.concurrent.ExecutionContext.Implicits.global
import akka.actor.{Actor, ActorSystem, Props}

object AgentDemo extends App {
  val as = ActorSystem("AgentListener")
  val agent = akka.agent.Agent(5)
  agent.send(23)
  val LisActor = as.actorOf(Props[ListeningActor], "Listener")
  // send data continously to the remote actor
  while (true) {
    val agentFuture = agent.future()
    agentFuture.onComplete {
      case scala.util.Success(e) => LisActor ! AgentData(e)
      case scala.util.Failure(e) => // don't send anything to the actor
    }
  }

  println("so")

}

case class AgentData(a: Int)

/**
  */
class ListeningActor extends Actor {
  def receive = {
    case AgentData(a) => println( s"""Received AgentData ${a}""")
    case _ => println("Unhandled message")
  }
}
