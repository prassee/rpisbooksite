package chapter7

import akka.actor.FSM
import akka.actor.Props
import scala.concurrent.ExecutionContext.Implicits.global
import java.io.File
import java.io.FileWriter

trait State



case object Init extends State

case object BurstAccept extends State

case object CalcBurstValues extends State

sealed trait Data

case object DummyData extends Data

case class CumulativeCalcData(data: Map[String, scala.collection.mutable.Seq[Int]], target: File) extends Data

sealed trait Messages

case object SessionStart extends Messages

case object SessionEnd extends Messages

case object BurstStart extends Messages

case object BurstEnd extends Messages

case class BurstData(symValue: Seq[(String, Int)]) extends Messages

/**
  *
  */
class AverageCalcFSM extends FSM[State, Data] {

  def mergeValues(a: Map[String, scala.collection.mutable.Seq[Int]], x: (String, Int)) = {
    a.get(x._1) match {
      case Some(e) => a.get(x._1).get.+:(x._2)
      case None => scala.collection.mutable.Seq(x._2)
    }
  }

  startWith(Init, DummyData)

  when(Init) {
    case Event(SessionStart, _) =>
      val cu = System.currentTimeMillis
      val session = new File( s"""/data/session_$cu""")
      session.createNewFile()
      goto(BurstAccept) using CumulativeCalcData(Map(), session)
  }

  when(BurstAccept) {
    case Event(BurstStart, a: CumulativeCalcData) => goto(CalcBurstValues)
  }

  when(CalcBurstValues) {
    case Event(bs: BurstData, a: CumulativeCalcData) =>
      val newData = for {
        x <- bs.symValue
      } yield x._1 -> mergeValues(a.data, x)
      val newCumData = a.copy(data = newData.toMap.++:(a.data))
      stay using newCumData

    case Event(BurstEnd, cumulativeData: CumulativeCalcData) =>
      val sumed = cumulativeData.data.map(x => x._1 -> x._2.sum / x._2.size)
      val line = sumed.foldLeft("")((b, a) => a._1 + ":" + a._2 + b + " ")
      val filewriter = new FileWriter(cumulativeData.target, true)
      filewriter.write(line)
      filewriter.write("\n")
      filewriter.close()
      stay

    case Event(SessionEnd, a: CumulativeCalcData) =>
      goto(Init) using DummyData
  }

  initialize
}

/**
  *
  */
object AverageRunner extends App {
  val avgRunner = akka.actor.ActorSystem("FSM")
  val fsm = avgRunner.actorOf(Props[AverageCalcFSM], "name")
  fsm ! SessionStart
  fsm ! BurstStart
  fsm ! BurstData(List(("a", 23), ("b", 30)))
  fsm ! BurstData(List(("a", 23), ("c", 30)))
  fsm ! BurstData(List(("c", 23), ("b", 30)))
  fsm ! BurstEnd
  fsm ! SessionEnd
}
