import Dependencies._

// factor out common settings into a sequence
lazy val buildSettings = Seq(
  organization := "rpisbooksite",
  version := "0.1.0",
  scalaVersion := "2.11.7"
)

lazy val root = Project(id = "chapter7", base = file(".")).
  settings(buildSettings: _*).
  settings(
    libraryDependencies ++= chapter7Dep,
    offline := true
)
