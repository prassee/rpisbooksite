import sbt._
import sbt.Keys._

object Dependencies {

  lazy val akkaVersion = "2.3.11"

  lazy val chapter7Dep = Seq(
    "com.typesafe.akka" %% "akka-actor" % akkaVersion,
    "com.typesafe.akka" %% "akka-agent" % akkaVersion
  )

}

