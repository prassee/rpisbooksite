package chapter3

import java.util.concurrent.TimeUnit
import scala.concurrent.Await
import scala.concurrent.Future
import scala.concurrent.ExecutionContext.Implicits._
import scala.concurrent.duration.Duration
import scala.util.Success
import scala.util.Failure

object StockSymbols extends Enumeration {
  type StockSymbols = Value
  val SUN, CSCO, ORA, IBM, GOGL, FCB = Value
}

case class StockQuote(sym: StockSymbols.StockSymbols, price: Int)

object StockExchange {
  val random = new java.util.Random()
  def getQuote(sym: StockSymbols.StockSymbols) = {
    val r = random.nextInt(99)
    Thread.sleep(r * 50)
    StockQuote(sym, r)
  }

  def isWorthyBuy(
    stock1: StockQuote,
    stock2: StockQuote
  ) = stock1.price > stock2.price match {
    case true => stock1
    case false => stock2
  }

  def buy(symbol: StockSymbols.StockSymbols, nos: Int) = {
    println(s"bought ${nos} ${symbol} shares")
  }
}

object FutureStockExchamgeDemo extends App {
  import StockSymbols._
  val ibmQuote = Future { StockExchange.getQuote(IBM) }
  val oraQuote = Future { StockExchange.getQuote(ORA) }

  val x = (a: StockQuote, b: StockQuote) => Future { StockExchange.isWorthyBuy(a, b) }
  val nos = 3

  val boughtSymbols = for {
    ibm <- ibmQuote
    ora <- oraQuote
    worthy <- x(ibm, ora)
  } yield StockExchange.buy(worthy.sym, nos)

}

object StockExchangeApp extends App {
  def composableStocks(nos: Int) = {
    import StockSymbols._
    val ibmQuote = Future { StockExchange.getQuote(IBM) }
    val oraQuote = Future { StockExchange.getQuote(ORA) }
    val x = (a: StockQuote, b: StockQuote) => Future { StockExchange.isWorthyBuy(a, b) }
    val boughtSymbols = for {
      i <- ibmQuote
      o <- oraQuote
      worthy <- x(i, o)
    } yield StockExchange.buy(worthy.sym, nos)

    boughtSymbols
  }
  composableStocks(5)
  Await.result(composableStocks(5), Duration(5, TimeUnit.SECONDS))

}
