package chapter3
/**
 *
 */
object WordCluster extends App {
  case class Word(docId: String, word: String)

  val doc1 = List(Word("d1", "the"), Word("d1", "while"), Word("d1", "an"))

  val doc2 = List(Word("d2", "the"), Word("d2", "then"), Word("d2", "when"))

  val x = doc1.map(x => x.docId -> x.word) ::: doc2.map(x => x.docId -> x.word)

  println(x.groupBy(x => x._2).map(x => x._1 -> x._2.map(x => x._1)))
}

/**
 * @author prasannakumar
 */
object DoMain extends App {
  
}