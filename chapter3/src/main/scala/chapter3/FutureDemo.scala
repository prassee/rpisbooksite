package chapter3

import java.util.concurrent.TimeUnit

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent._
import scala.concurrent.duration.Duration

/**
  *
  */
object FutureDemo extends App {

  val x = Future {
    Thread.sleep(1200)
    323
  }
  println("Waiting for the Furtue to complete")
  x.onComplete {
    case scala.util.Success(e) => println()
    case scala.util.Failure(e) => e.printStackTrace()
  }

  Thread.sleep(2000)

 //  Await.result(x, Duration(2, TimeUnit.MILLISECONDS))

  // Await.result(x, Duration(5, TimeUnit.SECONDS))
}
