package chapter3


import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.Success
import java.net.URL
import uk.co.bigbeeconsultants.http.HttpClient
import uk.co.bigbeeconsultants.http.response.Response

object AsyncHttpClient extends App {
  val httpClient = new HttpClient()

  def asyncGet(url: String) = {
    val response = Future(httpClient.get(new URL(url)))
    response.value
  }

  println(asyncGet("http://www.google.com"))
}
 
