package chapter3

import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.Failure
import scala.util.Success
import java.io.File
import scala.util.Failure
import scala.util.Failure

/**
 *
 */
object FileReadingFuture {

  println("FileReadingFuture")

  val filesath = new File(this.getClass.getResource("/automobile/101725").getPath)

  val fileReadingFut: Future[String] = Future {
    val file = scala.io.Source.fromFile(filesath)
    file.getLines().toList.mkString("\n")
  }

  fileReadingFut onComplete {
    case Success(t) => println(t)
    case Failure(e) => e.printStackTrace()
  }

  Thread.sleep(1000)
}

object PromiseDemo extends App {

  val x = Promise[Int]

  val fut = Future(23)

  fut.onComplete {

    case Success(e) => x.success(e)
    case Failure(e) => x.failure(e)
  }

  x.future.onComplete {
    case Success(e) => println(e)
    case Failure(e) => println(e)
  }

}
