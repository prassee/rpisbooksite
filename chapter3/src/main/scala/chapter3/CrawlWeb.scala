package chapter3

import java.io.File
import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.util.Success
import java.io.FileWriter
import scala.util.Failure

object CrawlWeb extends App {
  println("Future Driven Crawl")

  def extractWithRegex(regex: String, path: String): Future[(String, List[String])] =
    Future {
      val file = new java.io.File(path)
      val buffSource = scala.io.Source.fromFile(file)
      (path, regex.r.findAllIn(buffSource.getLines().mkString("\n")).toList)
    }
  
  val sgmFolder = new File("/data/bigDataSets/reuters21578/sgm")
  val regex = """\<BODY\>.+\n((.+)\n)+&#3;\<\/BODY\>"""
  val crawlFut = sgmFolder.listFiles().map { x => extractWithRegex(regex, x.getPath) }

  crawlFut.foreach(x => x.onComplete {
    case Success(data) => {
      println(data)
      //writeTitles(sgmFolder.getParent + "/data/index", data._1)
      //writeTitles(sgmFolder.getParent + "/data/titles", data._2.map { x => data._1 + "\t" + x }.mkString(" \n"))
    }
    case Failure(e) => println(e)
  })

  def writeTitles(path: String, data: String) = {
    val fw = new FileWriter(path, true)
    fw.append(data)
    fw.append("\n")
    fw.close()
  }

  Thread.sleep(5000)
}
