package chapter3

import scala.concurrent._
import ExecutionContext.Implicits.global
import scala.util.Failure
import scala.util.Success

/**
 *
 */
object DataGatheringFuture extends App {

  // this.getClass.getResource("/automobile/72052").getPath
  def fileReading(path: String) = Future {
    scala.io.Source.fromFile(path).getLines().mkString(" ")
  }

  def extractWithRegex(regex: String, path: String): Future[List[(String, String)]] =
    Future {
      val file = new java.io.File(path)
      val buffSource = scala.io.Source.fromFile(file)
      regex.r.findAllIn(buffSource.getLines().mkString("\n")).toList.map(x => file.getName -> x)
    }

  // reads and combines raw text
  /*val filesReadFut = for {
    f1 <- fileReading(this.getClass.getResource("/automobile/72052").getPath)
    f2 <- fileReading(this.getClass.getResource("/automobile/102616").getPath)
  } yield f1 + f2*/

  /*filesReadFut onSuccess {
    case results => println(results)
  }*/

  // reads and find text matching certain regex for each file

  // this extractWithRegex funciton does not gurantee Future ... 
  val filesReadWithRegexFut = for {
    f1 <- extractWithRegex("""(\w+)""", this.getClass.getResource("/automobile/72052").getPath)
    f2 <- extractWithRegex("""(\w+)""", this.getClass.getResource("/automobile/101725").getPath)
  } yield f1 ::: f2

  filesReadWithRegexFut onComplete {
    case Success(text) => println(text.groupBy(x => x._2).map(x => x._1 -> x._2.map(x => x._1).toSet))
    case Failure(e) => e.printStackTrace()
  }

  Thread.sleep(3000)
}
