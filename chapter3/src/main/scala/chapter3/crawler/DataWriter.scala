package crawler

import java.io.FileWriter
import java.io.File

// using type class create a write data to file behavior for JSON and plain text
sealed trait Crawl
case class JsonCrawl(label: String, title: String) extends Crawl
case class PlainTxtCrawl(label: String, title: String) extends Crawl

// type class 
trait DataWriter[T] {
  println(s"""Datawrite Intialized with ${"23"}""")
  def write(cd: T): Unit
}

object DataWriterApp {
  val outputPath = this.getClass.getResource("/output").getPath
  // type Instances of DataWriter 
  implicit object JSONDataWriter extends DataWriter[JsonCrawl] {
    def write(cd: JsonCrawl) = {
      val jsonDoc = s"""{"fileId" : ${cd.label} , "title" : ${cd.title}}"""
      flush(cd.label, jsonDoc, ".json")
    }
  }

  implicit object PlainDataWriter extends DataWriter[PlainTxtCrawl] {
    def write(cd: PlainTxtCrawl) = {
      val plainTxt = s""" "fileId" -> ${cd.label} , "title" -> ${cd.title} """
      flush(cd.label, plainTxt, ".txt")
    }
  }

  def write[T](cd: T)(implicit writer: DataWriter[T]) = writer.write(cd)

  private def flush(fileId: String, data: String, ext: String) = {
    val fw = new FileWriter(new File(outputPath + "/" + fileId.dropRight(5) + ext))
    fw.write(data)
    fw.close()
  }

}
