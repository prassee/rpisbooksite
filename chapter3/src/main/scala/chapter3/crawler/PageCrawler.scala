package crawler

import java.io.File

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future
import scala.io.{ Codec, Source }
import scala.util.{ Failure, Success }
import scala.xml.NodeSeq

case class CrawlData(fileId: String, title: String)

object PageCrawler extends App {
  val rssSources = Array("/rss/rss1.xml")
  rssSources.foreach { rss => new PageCrawler(rss).crawl }
  Thread.sleep(1000)
}

class PageCrawler(rssPath: String) {
  def crawl = {
    val loc = this.getClass().getResource(rssPath).getPath
    val fut = RssReader(loc).extractOnlyUrls
    fut.onComplete {
      case Success(e) => e.foreach { x => dumpData(x) }
      case Failure(e) => e.printStackTrace()
    }
  }

  def dumpData(cd: CrawlData) = {
    import crawler.DataWriterApp._
    val x = JsonCrawl(cd.fileId, cd.title)
    write(x)
    val y = PlainTxtCrawl(cd.fileId, cd.title)
    write(y)
  }
}

object RssReader {
  def apply(url: String) = {
    val rssSource = Source.fromFile(new File(url))
    val rssLines = rssSource.getLines().mkString
    val xml = scala.xml.XML.loadString(rssLines)
    val allURLs: NodeSeq = xml \ "rss" \ "channel" \ "item" \ "guid"
    new RssReader(url, allURLs)
  }
}

class RssReader(basePath: String, links: NodeSeq) {
  def extractOnlyUrls = {
    val urls: Seq[File] = links.map { x => new File(basePath.dropRight(8) + "../webpages/" + x.text) }
    val files = urls.map { x => Future { new TitleReader(x).readTitle } }.toList
    Future.sequence(files)
  }
}

class TitleReader(file: File) {
  val regx = """.*<title>(.+)</title>.*""".r
  def readTitle: CrawlData = {
    val x = Source.fromFile(file)(Codec.ISO8859)
    val crawledData = x.getLines().mkString
    val title = crawledData match {
      case regx(y) => y
      case _       => "no-title found"
    }
    CrawlData(file.getName, title)
  }
}

