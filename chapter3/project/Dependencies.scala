import sbt._
import sbt.Keys._

object Dependencies {
 lazy val chapter3Dep = Seq(
    "org.scala-lang" % "scala-xml" % "2.11.0-M4",
   "uk.co.bigbeeconsultants" %% "bee-client" % "0.28.0",
    "org.slf4j" % "slf4j-api" % "1.7.+",
    "ch.qos.logback" % "logback-core" % "1.0.+",
    "ch.qos.logback" % "logback-classic" % "1.0.+")

}

